#!/bin/bash

while true
do
    DATE=$(date +"%M");
    if [ $DATE -eq $2 ]
    then
        notify-send $1
    fi
    sleep 60
done
